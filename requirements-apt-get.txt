python-pip
libxml2-dev
libxslt1-dev
python-dev
cython # for lxml
zlib1g-dev
git # for vcs (code repository)

python3-dev # if using python 3

libffi-dev # bpython

python-virtualenv
libssl-dev
sqlite3
