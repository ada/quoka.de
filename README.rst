install apt-get requirements
============================

    sudo apt-get install -y --force-yes $(cat requirements-apt-get.txt | grep -oP "^[^#\s]+")

create python virtualenv
========================

    sudo virtualenv -p python2 /opt/quoka.de

create python requirements
==========================

    sudo /opt/quoka.de/bin/pip install --upgrade pip
    sudo /opt/quoka.de/bin/pip install setuptools==18.5
    sudo /opt/quoka.de/bin/pip install -r requirements-pip.txt


run spider
==========

    cd /home/bitbucket.org/quoka.de/scrofy/scrofy
    . /opt/quoka.de/bin/activate
    export PYTHONPATH=/home/bitbucket.org/quoka.de/
    scrapy crawl quoka.de1 && scrapy crawl quoka.de2

    # found 4744 adverts
    # after removing duplicated by unique id got 2177 noise (Poland, Swiss, France zip codes)

run with docker
===============

    $ docker build -t lot_internet .
    $ docker run lot_internet bash -c "
        cd /app/scrofy/scrofy
        . /opt/quoka.de/bin/activate
        export PYTHONPATH=/app/
        mv /app/scrofy/scrofy/quoka.sqlite /app/scrofy/scrofy/quoka.sqlite.backup.`date +%s`
        mv /app/scrofy/scrofy/REPOs.repr /app/scrofy/scrofy/REPOs.repr.backup.`date +%s`
        scrapy crawl quoka.de1 && scrapy crawl quoka.de2
    "
    2016-08-22 09:24:12 [quoka.repo] WARNING: expecting German prefix, got 'F -', this record will be skipped
    2016-08-22 09:24:50 [quoka.repo] WARNING: expecting German prefix, got 'PL -', this record will be skipped
    2016-08-22 09:29:43 [quoka.repo] WARNING: expecting German prefix, got 'CZ -', this record will be skipped
    ...

    # get container ID
    $ docker ps -a | grep "lot_internet" | head -2 | grep -oP "^[0-9a-f]+ "
    d4748ff0a7bc

    # copy quoka.sqlite
    # docker cp <containerId>:/app/scrofy/scrofy/quoka.sqlite quoka.sqlite
    $ docker cp d4748ff0a7bc:/app/scrofy/scrofy/quoka.sqlite quoka.sqlite

    $ sqlite3 quoka.sqlite "select count(1) from lot_internet"
    2276



About "Immobilientyp_detail" aka Partner-Anzeige
================================================

All information can be found in api call.

    $ curl 'http://www.quoka.de/qs/qpc/xmlSearch.php?view=quoka&maxresults=200&country=D&city=Hamburg&output=json' | json_pp
  %
        {
           "boxHeadline" : "weitere Angebote",
           "boxMoreAdsUrl" : "",
           "infoAds" : "",
           "result" : [
              {
                 "locationZipCode" : "",
                 "shop" : "grillstyle.de",
                 "priceCurrency" : "EUR",
                 "description" : "",
                 "priceShipping" : "-1",
                 "priceProduct" : "20.6",
                 "locationCity" : "",
                 "title" : " 's Grillen - Neue Rezepte für jeden Tag Deutschland, WEBER",
                 "priceTotal" : "20,60",
                 "urlClick" : "http://www4.mupads.de/solute.......",
                 "urlImage" : "http://aimg.billiger.de/ca/o/14780/470678662.jpg",
                 "pricePrefix" : "ab",
                 "locationCountry" : ""
                ...

