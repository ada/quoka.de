FROM ubuntu:14.04

MAINTAINER andrei.danciuc@asta-s.eu

COPY . /app
WORKDIR /app

RUN apt-get update && \
    (apt-get install -y --force-yes $(cat requirements-apt-get.txt | grep -oP "^[^#\s]+")) && \
    virtualenv -p python2 /opt/quoka.de && \
    /opt/quoka.de/bin/pip install --upgrade pip && \
    /opt/quoka.de/bin/pip install setuptools==18.5 && \
    /opt/quoka.de/bin/pip install -r requirements-pip.txt
