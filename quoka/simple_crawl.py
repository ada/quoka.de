"""
pragmatic solution for crawling without framework like scrapy
"""
import logging
import time
from datetime import timedelta

import requests
from ciur import bnf_parser, parse, open_file
from ciur.rule import Rule
from slugify import slugify

from quoka import xpath_functions
from quoka.quoka_types import QuokaOfferType, QuokaProvider
from quoka.repo import Repo
from quoka.repo import insert_quoka_into_lot_internet

HTTP = requests.Session()
LOG = logging.getLogger(__name__)


bnf_parser.casting_modules.add(xpath_functions)

WELCOME_PAGE_RULE = Rule.from_dict(bnf_parser.external2dict(open_file(
    "quoka.de_welcome_page.ciur", __file__
))[0])

ADVERTS_PAGE_RULE = Rule.from_dict(bnf_parser.external2dict(open_file(
    "quoka.de_adverts_page.ciur", __file__
))[0])

DETAILED_PAGE_RULE = Rule.from_dict(bnf_parser.external2dict(open_file(
    "quoka.de_detailed_page.ciur", __file__
))[0])


def get_guoka_cities():
    """
    get list of cities from guoka site
    http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c2710a172122841/mallorca-arbeiten-andere-urlaub.html
    """
    welcome_page_url = "http://www.quoka.de/immobilien/bueros-gewerbeflaechen"
    response = HTTP.get(welcome_page_url)
    data = parse.html_type(
        parse.Document(response.content), WELCOME_PAGE_RULE
    )

    return data["cities"]


def adverts(city, provider, offer_type, page_number):
    """
    :param city: {"url": ..., "name",}
    :param provider: QuokaProvider
    :param offer_type: QuokaOfferType
    :param page_number:
    :return:
    """
    post_data = {
        "classtype": offer_type.value,
        "comm": provider.value,
        "pageno": page_number,
        "country": "D",
        "city": city["name"],
        "radius": 100
    }

    response = HTTP.post(city["url"], post_data)

    data = parse.html_type(
        parse.Document(response.content), ADVERTS_PAGE_RULE
    )

    return data


def collect_mate_data(repo):
    """
    get only shot info about advert
    """
    start_time = time.time()

    for city in get_guoka_cities():
        LOG.info("parsing city %r", city["name"])

        for provider in (
                QuokaProvider.private,
                QuokaProvider.commercial,
        ):
            next_page = 1
            while next_page:
                LOG.info("page %r, provider %r", next_page, provider)
                time.sleep(0.5)
                data = adverts(
                    city,
                    provider,
                    QuokaOfferType.sale,
                    next_page,
                )

                next_page = data["root"].get("next_page")

                reach_advert_list = data["root"].get("reach_advert_list", [])
                poor_advert_list = data["root"].get("poor_advert_list", [])

                assert reach_advert_list or poor_advert_list

                if reach_advert_list:
                    repo.generic_save("advert", reach_advert_list)

                if poor_advert_list:
                    repo.generic_save("advert", poor_advert_list)

    finish_time = time.time()
    spent_time = timedelta(seconds=finish_time - start_time)

    LOG.info(
        "got %r adverts references in %s",
        repo.count(repo.ADVERT),
        spent_time,
    )


def collect_telephones(telephone_url_list):
    """
    from
    http://www.quoka.de//ajax/detail/displayphonenumber.php?coded=MDYyMS8gMTIyODQyMTg-&adno=164684110&adsource=quoka+online&catid=27_2710&cusno=24188326
    get
    <span class="cust-type">0621/ 12284218</span>
    but extract
    0621/ 12284218
    """

    import re
    results = []
    for url in telephone_url_list:
        time.sleep(0.5)
        response = HTTP.get(url)
        telephone_number = re.search(r">\s*(.*?)\s*<", response.content)
        results.append(telephone_number.group(1))

    return results


def collect_details(repo):
    """
    get detailed information about advert
    """
    LOG.info("fill details ")
    start_time = time.time()
    for item in repo.find(repo.ADVERT):
        if "url" in item:
            url = "http://www.quoka.de" + item["url"]
        else:
            slugify_input = item.get("slugify_input")
            if not slugify_input:
                item["slugify_input"] = slugify(item["title"])

            url = "http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c{category_id}a{unique_id}/{slugify_input}.html".format(**item)  # pylint: disable=line-too-long

        time.sleep(1)
        response = HTTP.get(url)
        data = parse.html_type(
            parse.Document(response.content), DETAILED_PAGE_RULE
        )

        product = data["product"]
        if "date" in item:
            product["title"] = item["title"]

        product["url"] = url

        item["product"] = product
        repo.save(repo.ADVERT, item)

    finish_time = time.time()
    spent_time = timedelta(seconds=finish_time - start_time)
    LOG.info(
        "done in %s",
        spent_time
    )


def main():
    """
    entry point
    """

    repo = Repo()
    collect_mate_data(repo)
    collect_details(repo)
    products_telethon_url_to_number(repo)
    import_quoka_into_lot_internet(repo)


def import_quoka_into_lot_internet(repo):
    """
    convert parsed advert from document format into LotInternet format
    save it in sqlite database
    """
    for item in repo.find(repo.ADVERT):
        product = item["product"]

        insert_quoka_into_lot_internet(product)


def products_telethon_url_to_number(repo):
    """
    telephone number is obfuscated on site,
    in order to find out telephone number there is a need to to
    additional ajax call
    """
    for item in repo.find(repo.ADVERT):
        product = item["product"]
        telephone_url_list = product.pop("telephone_url_list", None)
        if telephone_url_list:
            telephones = collect_telephones(telephone_url_list)
            product["telephone_number_list"] = telephones
            repo.save(repo.ADVERT, item)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main()
