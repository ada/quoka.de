"""
Database models Repo
LOT_INTERNET ORM
"""
import os
import re
import logging
import copy

# for serialise
from collections import OrderedDict  # pylint: disable=unused-import
import datetime

from sqlalchemy import create_engine
from sqlalchemy.sql.sqltypes import Integer, DateTime, String, Float
from sqlalchemy.sql.schema import Table, Column, MetaData, UniqueConstraint
import sqlalchemy.exc


LOG = logging.getLogger(__name__)


def import_repr(path):
    """
    get python repr(value) saved as a string and revert back to python
    """
    with open(path, "r") as file_cursor:
        return eval(file_cursor.read(), globals(), locals())  # pylint: disable=eval-used


def export_repr(path, value):
    """
    get python repr(value) and save it  as a string into a file
    """
    with open(path, "w") as file_cursor:
        file_cursor.write(repr(value))


class Repo(object):
    """
    inmmemory implementation of database repository
    """
    ADVERT = "advert"
    CITY = "city"

    def __init__(self, storage_file="REPOs.repr"):

        self._storage_file = storage_file
        if os.path.exists(self._storage_file):
            self._storage = import_repr(self._storage_file)
        else:
            self._storage = {
                self.ADVERT: {},
                self.CITY: {},
            }

    def save(self, name, item):
        """
        upsert an item
        """
        if name == self.ADVERT:
            self._storage[name][item["unique_id"]] = copy.copy(item)
            return item["unique_id"]

        if name == self.CITY:
            self._storage[name][item["name"]] = copy.copy(item)
            return item[name]

        raise NotImplementedError

    def save_batch(self, name, items):
        """
        save multiple elements
        """
        ids = []
        for item in items:
            id_ = self.save(name, item)
            ids.append(id_)

        return ids

    def generic_save(self, name, item):
        """
        in case we don't know that we have one or a list of element
        """
        if isinstance(item, list):
            return self.save_batch(name, item)

        return self.save(name, item)

    def count(self, name):
        """
        get total number of items in database
        """
        # TODO: implement filters
        return len(self._storage[name])

    def find(self, name):
        """
        find all elements and return them
        """
        # TODO: implement filters
        return self._storage[name].values()

    def __del__(self):
        export_repr(self._storage_file, self._storage)

    def get(self, name, unique_id):
        """
        get element from db by its id
        """
        return self._storage[name][unique_id]


DB = create_engine('sqlite:///quoka.sqlite')

DB.echo = False  # Try changing this to True and see what happens

LOT_INTERNET = Table('lot_internet', MetaData(DB),
                     Column('id', Integer, primary_key=True),
                     Column('Boersen_ID', Integer),
                     Column('OBID', Integer),
                     Column('erzeugt_am', DateTime),
                     Column('Anbieter_ID', Integer),
                     Column('Anbieter_ObjektID', String),
                     Column('Immobilientyp', String),
                     Column('Immobilientyp_detail', String),
                     Column('Vermarktungstyp', String),
                     Column('Land', String),
                     Column('Bundesland', String),
                     Column('Bezirk', String),
                     Column('Stadt', String),
                     Column('PLZ', String),
                     Column('Strasse', String),
                     Column('Hausnummer', String(40)),
                     Column('Uberschrift', Integer),
                     Column('Beschreibung', String),
                     Column('Etage', Integer),
                     Column('Kaufpreis', Float),
                     Column('Kaltmiete', Float),
                     Column('Warmmiete', Float),
                     Column('Nebenkosten', Float),
                     Column('Zimmeranzahl', Float),
                     Column('Wohnflaeche', Float),
                     Column('Monat', Integer),
                     Column('url', String),
                     Column('Telefon', String),
                     Column('Erstellungsdatum', Integer),
                     Column('Gewerblich', Integer),

                     UniqueConstraint('Boersen_ID', 'OBID')
                    )

try:
    LOT_INTERNET.create()
except (sqlalchemy.exc.OperationalError, ) as exc:
    if "table lot_internet already exists" not in str(exc):
        raise


def insert_quoka_into_lot_internet(product):
    """
    insert one document into lot_internet storage
    """
    assert product["currency"] == "EUR", \
        "expecting EUR, got {}".format(product["currency"])

    if product["zip"]["prefix"] != "D -":
        # there ar "P" and "F" in results also
        LOG.warn(
            "expecting German prefix, got %r, this record will be skipped",
            product["zip"]["prefix"]
        )
        return

    sql_context = LOT_INTERNET.insert()

    return sql_context.execute({
        "Boersen_ID": 21,
        "Uberschrift": product["title"],
        "Kaufpreis": product["price"],
        "PLZ": str(product["zip"]["value"]),
        "Stadt": product["locality"],
        "OBID": product["unique_id"],
        "Erstellungsdatum": product["date"],
        "Telefon": product.get("telephone_number_list", [None])[0],
        "Beschreibung": re.sub(r"\s+", " ", product["description"]),
        "Anbieter_ID": product.get("provider_id"),
        "erzeugt_am": product.get(
            "crawling_create_date", datetime.datetime.utcnow()
        ),
        "Monat": datetime.datetime.utcnow().month,
        "url": product.get("url"),
        "Gewerblich": int(product.get("is_commercial_provider", False))
    })
