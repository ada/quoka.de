"""
custom methods on top of `ciur` DSL parser
"""
import datetime
import re

from dateutil.relativedelta import relativedelta
from slugify import slugify
from ciur.decorators import check_new_node
from ciur.helpers import load_xpath_functions
from ciur import xpath_functions_ciur


@check_new_node
def fn_quoka_datetime(context, value):
    """
    handle datetime conversion for German specific human time
    """
    del context

    tail = value.tail.strip().lower()
    if tail:
        if tail == "gestern":
            return datetime.datetime.utcnow() - datetime.timedelta(days=-1)

        match = re.search(r"vor (\d+) monaten", tail, re.IGNORECASE)
        if match:
            months = int(match.group(1))
            return (
                datetime.datetime.utcnow() - relativedelta(months=-months)
            )

        value = tail
    else:
        following_sibling = value.getnext()
        value = following_sibling.text

    value = value.replace("Heute, ", "").replace(" Uhr", "")
    return xpath_functions_ciur.fn_datetime(None, value)


@check_new_node
def fn_quoka_datetime2(context, value):
    """
    timestamp to datetime
    """
    del context
    if isinstance(value, int):
        return datetime.datetime.utcfromtimestamp(value)

    raise NotImplementedError


def fn_quoka_slugify(context, value):
    """
    There is a need to slugify `quoka` url components in order to look like an
    ordinary user.
    """
    del context
    return slugify(value)

load_xpath_functions(locals())
