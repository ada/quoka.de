"""
internal data types for Quoka module
"""
from enum import Enum


class QuokaOfferType(Enum):  # pylint: disable=too-few-public-methods
    """
    classtype
    """
    sale = "of"
    requests = "wa"
    all = "all"


class QuokaProvider(Enum):  # pylint: disable=too-few-public-methods
    """
    comm
    """
    commercial = "1"
    private = "0"
    all = "all"
