"""
quoka scraper on top on scrapy
"""
import re

import scrapy
from ciur import parse
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from slugify import slugify

from quoka.quoka_types import QuokaProvider, QuokaOfferType
from quoka.repo import Repo
from quoka.simple_crawl import WELCOME_PAGE_RULE, ADVERTS_PAGE_RULE, \
    DETAILED_PAGE_RULE, import_quoka_into_lot_internet

REPO = Repo()
count = 0


class QuokaSpider1(scrapy.Spider):
    """
    First spider - collect short info about adverts and remove duplicate
    """
    name = "quoka.de1"
    allowed_domains = ["quoka.de"]
    start_urls = (
        'http://www.quoka.de/immobilien/bueros-gewerbeflaechen',
    )

    def __init__(self, *args, **kwargs):
        super(QuokaSpider1, self).__init__(*args, **kwargs)
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        """
        close listener for scrapy
        """
        global REPO
        del REPO  # triggering destructor call, to save all data on disk

    def parse(self, response):
        data = parse.html_type(
            parse.Document(response.body), WELCOME_PAGE_RULE
        )

        for city in data["cities"]:
            for provider in (
                    QuokaProvider.private,
                    QuokaProvider.commercial,
            ):
                next_page = 1
                yield scrapy.FormRequest(
                    url=city["url"],
                    method="POST",
                    formdata={
                        "classtype": QuokaOfferType.sale.value,
                        "comm": provider.value,
                        "pageno": str(next_page),
                        "country": "D",
                        "city": city["name"],
                        "radius": "100"
                    },
                    meta={
                        "url": city["url"],
                        "provider": provider.value,
                        "city_name": city["name"],
                    },
                    callback=self.parse_adverts
                )

    def parse_adverts(self, response):
        """
        fetch short info about all available adverts
        """
        global count
        data = parse.html_type(
            parse.Document(response.body), ADVERTS_PAGE_RULE
        )

        reach_advert_list = data["root"].get("reach_advert_list", [])
        count += len(reach_advert_list)
        print("==== reach_advert_list", len(reach_advert_list))

        poor_advert_list = data["root"].get("poor_advert_list", [])
        count += len(poor_advert_list)
        print("==== poor_advert_list", len(poor_advert_list))

        print("count =", count)

        if not (reach_advert_list or poor_advert_list):
            raise NotImplementedError

        if reach_advert_list:
            REPO.generic_save("advert", reach_advert_list)

        if poor_advert_list:
            REPO.generic_save("advert", poor_advert_list)

        next_page = data["root"].get("next_page")
        if next_page:
            yield scrapy.FormRequest(
                url=response.meta["url"],
                method="POST",
                formdata={
                    "classtype": QuokaOfferType.sale.value,
                    "comm": response.meta["provider"],
                    "pageno": str(next_page),
                    "country": "D",
                    "city": response.meta["city_name"],
                    "radius": "100"
                },
                meta={
                    "next_page": next_page,
                    "url": response.meta["url"],
                    "provider": response.meta["provider"],
                    "city_name": response.meta["city_name"],
                },
                callback=self.parse_adverts
            )


class QuokaSpider2(QuokaSpider1):
    """
    Second spider - fetch full info for adverts, get telephones, and
    save result into LotInternet format
    """
    name = "quoka.de2"

    def spider_closed(self, spider):
        """
        close listener, save data on close as LotInternet format
        """
        global REPO
        import_quoka_into_lot_internet(REPO)
        del REPO

    def parse(self, response):
        # parse details for each advert
        for item in REPO.find(Repo.ADVERT):
            if "url" in item:
                url = "http://www.quoka.de" + item["url"]
            else:
                slugify_input = item.get("slugify_input")
                if not slugify_input:
                    item["slugify_input"] = slugify(item["title"])

                url = "http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c{category_id}a{unique_id}/{slugify_input}.html".format(
                    **item)

            print(">>> url ", url)

            yield scrapy.Request(
                url=url,
                method="GET",
                callback=self.parse_details,
                meta={"item": item}
            )

    def parse_details(self, response):

        data = parse.html_type(
            parse.Document(response.body), DETAILED_PAGE_RULE
        )

        item = response.meta["item"]
        product = data["product"]
        if "date" in item:  # it is a reach advert, has timestamp in html attr
            product["title"] = item["title"]

        product["url"] = response.url

        item["product"] = product
        REPO.save(Repo.ADVERT, item)

        telephone_url_list = product.pop("telephone_url_list", None)

        if telephone_url_list:
            for telephone_url in telephone_url_list:
                yield scrapy.Request(
                    url=telephone_url,
                    method="GET",
                    meta={
                        "unique_id": item["unique_id"]
                    },
                    callback=self.parse_telephone
                )

    def parse_telephone(self, response):

        telephone_number = re.search(r">\s*(.*?)\s*<", response.body)
        item = REPO.get(Repo.ADVERT, response.meta["unique_id"])
        telephone_number_list = item["product"].get("telephone_number_list", [])
        telephone_number_list.append(telephone_number.group(1))

        item["product"]["telephone_number_list"] = telephone_number_list

        if response.url in item["product"].get("telephone_url_list", []):
            item["product"]["telephone_url_list"].remove(response.url)

        REPO.save(Repo.ADVERT, item)
