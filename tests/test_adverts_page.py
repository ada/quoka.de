import requests
from ciur import bnf_parser, parse, pretty_json, open_file
from ciur.rule import Rule

from quoka import xpath_functions

requests = requests.Session()

bnf_parser.casting_modules.add(xpath_functions)


def test_adverts():
    content = open("page_3.html").read()

    res = bnf_parser.external2dict(open_file(
        "../quoka/quoka.de_adverts_page.ciur", __file__
    ))
    rule = Rule.from_dict(res[0])  # doctest: +NORMALIZE_WHITESPACE
    data = parse.html_type(parse.Document(content), rule)
    print pretty_json(data)  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS



