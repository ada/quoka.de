import requests
from ciur import bnf_parser, parse, pretty_json, open_file
from ciur.rule import Rule

from quoka import xpath_functions

requests = requests.Session()

bnf_parser.casting_modules.add(xpath_functions)


def test_welcome(url="http://www.quoka.de/immobilien/bueros-gewerbeflaechen"):
    response = requests.get(url)
    # print (response.content)

    res = bnf_parser.external2dict(open_file(
        "../quoka/quoka.de_welcome_page.ciur", __file__
    ))
    rule = Rule.from_dict(res[0])  # doctest: +NORMALIZE_WHITESPACE
    data = parse.html_type(parse.Document(response.content), rule)
    print pretty_json(data)  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS

#test_welcome("http://www.quoka.de/immobilien/bueros-gewerbeflaechen")

