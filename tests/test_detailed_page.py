import requests
from ciur import bnf_parser, parse, pretty_json, open_file
from ciur.rule import Rule

from quoka import xpath_functions

requests = requests.Session()

bnf_parser.casting_modules.add(xpath_functions)


def test_details(url):
    response = requests.get(url)
    # print (response.content)

    res = bnf_parser.external2dict(open_file(
        "../quoka/quoka.de_detailed_page.ciur", __file__
    ))
    rule = Rule.from_dict(res[0])  # doctest: +NORMALIZE_WHITESPACE
    data = parse.html_type(parse.Document(response.content), rule)
    print pretty_json(data)  # doctest: +NORMALIZE_WHITESPACE, +ELLIPSIS

test_details("http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c2710a160773906/*.html")
test_details("http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c2710a172829708/*.html")
test_details("http://www.quoka.de/immobilien/bueros-gewerbeflaechen/c2710a172317402/*.html")


